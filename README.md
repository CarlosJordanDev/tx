# tx

A minimal modern-ish C++ console based text editor. Designed to be hacked, embedded, used to make your own lisp, or to learn about UNIX terminal IO. Code is deliberately simple. If you know a way to simplify it further, please open a pull request!

Released under the [MIT](LICENSE) license.

## Building
```
cd tx
mkdir build
cd build
cmake ..
make
```

## Running
```
tx filename
```

## Saving and Quitting
Enter `ctrl-s` to save, and `ctrl-q` to quit.