#define PY_SSIZE_T_CLEAN
#include <Python.h>
// ^ these need to stay before any system includes.
#include "editor.hpp"

using namespace ec;

extern "C" { 

// Tx.Editor Python Object
struct EditorObject {
    PyObject_HEAD
    Editor impl;
};

static PyObject * Editor_receive_key(PyObject * self, PyObject *Py_UNUSED(ignored)) {
    EditorObject * editor = (Editor*)self;
    return Py_None;
}

static PyObject * Editor_string_value(PyObject * self, PyObject *Py_UNUSED(ignored)) {
    return Py_None;
}

static PyObject * Editor_render_to_stream(PyObject * self, PyObject *args) {
    return Py_None;
}

static PyObject * Editor_new(PyTypeObject *type, PyObject *args, PyObject *kwds) {
    EditorObject * editor = new (type->tp_alloc(type, 0)) EditorObject;
    static char * kwlist[] = {"keyword_param"};
    int bool_keyword = 0;
    if (not PyArg_ParseTupleAndKeywords(args, kwds, "|p", kwlist, &bool_keyword)) {
    	return NULL;
    }
    if (not bool_keyword) {
	// Some flag
    }
    return reinterpret_cast<PyObject*>(editor);
}

static void Editor_dealloc(EditorObject * self) {
    self->~EditorObject();
}

static PyMethodDef Editor_methods[] = {
    {"do_something", 	 Editor_do_something, 	  METH_VARARGS, "docstring goes here"},
    {"do_another_thing", Editor_do_another_thing, METH_VARARGS, "docstring goes here"},
    {NULL, NULL, 0, NULL}
};

static PyTypeObject Editor_type = {
    PyVarObject_HEAD_INIT(NULL, 0)
};
void build_Editor_type() {
    Editor_type.tp_name 	= "terminal.Editor";
    Editor_type.tp_basicsize 	= sizeof(Editor);
    Editor_type.tp_dealloc 	= (destructor)Editor_dealloc;
    Editor_type.tp_flags 	= Py_TPFLAGS_DEFAULT;
    Editor_type.tp_methods 	= Editor_methods;
    Editor_type.tp_new 		= Editor_new;
}

// Tx Python Module
static PyModuleDef module = {
    PyModuleDef_HEAD_INIT
};
void build_module() {
    module.m_name = "editor";
    module.m_doc  = "Documentation for `editor`.";
    module.m_size = -1;
}

PyMODINIT_FUNC PyInit_editor() {
    if (PyType_Ready(&Raw_mode_type) < 0) {
    	return NULL;
    }
    build_module();
    PyObject * py_module = PyModule_Create(&module);
    if (py_module == NULL) {
    	return NULL;
    }
    Py_INCREF(&Editor_type);
    if (PyModule_AddObject(py_module, "Editor", (PyObject*)&Editor_type) < 0) {
    	Py_DECREF(&Editor_type);
	Py_DECREF(&module);
	return NULL;
    }
    return py_module;
}

} // extern "C"
