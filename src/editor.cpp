#include "editor.hpp"
#include "terminal.hpp"
#include <unistd.h>
#include <fstream>
#include <sstream>
#include <algorithm>
#include <numeric>

using namespace ec;
using namespace std;
    
void Editor::set_term(Terminal& term) {
    auto p = term.get_size();
    screen_size.x = p.first;
    screen_size.y = p.second - 2;
}

void Editor::open(string const& path) {
    filename = path;
    ifstream file{ filename };
    if (not file) {
        throw std::runtime_error("Unable to open file `" + path + "`.");
    }
    dirty = false;
    lines.clear();
    for (string line; getline(file, line); ) {
        insert_line(end(lines), (line.back() == '\n' or line.back() == '\r')
                                ? line.substr(0, line.size() - 1) : line);
    }
}

void Editor::save(string const& as_path) {
    ofstream save_file(as_path);
    if (save_file) {
        save_file << lines_to_string();
        filename = as_path;
    }
}

void Editor::propagate_open_comment(Line_iter line) {
    const auto is_open = line->has_open_comment();
    const bool knew_status = line->comment_is_open == is_open;
    line->comment_is_open = is_open;
    if (not knew_status and distance(begin(lines), line) + 1 < lines.size()) {
        (++line)->update_syntax(syntax);
        propagate_open_comment(line);
    }
}

void Editor::update_line(Line_iter line) {
    line->update();
    line->update_syntax(syntax);
}

void Editor::insert_line(Line_iter at, string const& s) {
    update_line(lines.insert(at, Line{s}));
}

string Editor::lines_to_string() {
    return accumulate(begin(lines), end(lines), string{},
                      [](auto &&n, const auto &r) {
                          n.append(r.chars);
                          n.append("\n");
                          return move(n);
                      });
}

void Editor::set_status_message(string const& msg) {
    statusmsg = msg;
    statusmsg_time = time(nullptr);
}

void Editor::move_right() {
    if (cursor.x == screen_size.x - 1) {
        ++offset.x;
    } else {
        ++cursor.x;
    }
}
void Editor::move_left() {
    if (cursor.x == 0) {
        offset.x -= min(1, offset.x);
    } else {
        --cursor.x;
    }
}
void Editor::move_down() {
    if (cursor.y == screen_size.y - 1) {
        ++offset.y;
    } else {
        ++cursor.y;
    }
}
void Editor::move_up() {
    if (cursor.y == 0) {
        offset.y -= min(1, offset.y);
    } else {
        --cursor.y;
    }
}
int Editor::xloc() const { return offset.x + cursor.x; }
int Editor::yloc() const { return offset.y + cursor.y; }

void Editor::insert_char(int c) {
    const size_t y = yloc();
    fill_n(back_inserter(lines), y + 1 - min(y + 1, lines.size()), empty_line);
    auto line = begin(lines) + y;
    line->insert_char(xloc(), c);
    update_line(line);
    move_right();
}

void Editor::insert_newline() {
    if (yloc() > lines.size()) {
        return;
    } else if (yloc() == lines.size()) {
        lines.insert(end(lines), empty_line);
    } else { // if (y < lines.size()) {
        auto line = begin(lines) + yloc();
        if (xloc() == 0) {
            lines.insert(begin(lines) + yloc(), empty_line);
        } else {
            const string first = line->chars.substr(0, xloc());
            const string second = line->chars.substr(xloc());
            line = lines.insert(line + 1, Line{second});
            update_line(line);
            (--line)->chars = first;
            update_line(line);
        }
    }
    move_down();
    cursor.x = 0;
    offset.x = 0;
}

void Editor::erase_char() {
    if ((xloc() == 0 and yloc() == 0) or yloc() >= lines.size()) {
        return;
    } else if (xloc() == 0) {
        const auto prev_line = begin(lines) + yloc() - 1;
        const auto line = prev_line + 1;
        const auto prev_len = prev_line->chars.size();
        prev_line->chars += line->chars;
        update_line(prev_line);
        lines.erase(line);
        move_up();
        cursor.x = prev_len;
        offset.x += xloc() - min(offset.x + screen_size.x, xloc());
    } else {
        const auto line = begin(lines) + yloc();
        line->chars.erase(begin(line->chars) + xloc() - 1);
        update_line(line);
        move_left();
    }
}

void Editor::select_syntax_highlight(string const& filename, vector<Syntax> const& syntax_db) {
    if (const auto i = find_if(
            begin(syntax_db), end(syntax_db),
            [&](auto const &syntax) {
                return find_if(begin(syntax.file_extensions),
                               end(syntax.file_extensions),
                               [&](auto const &ext) {
                                   return filename.size() >= ext.size() and
                                          equal(rbegin(ext), rend(ext),
                                                rbegin(filename));
                               }) != end(syntax.file_extensions);
            });
        i != end(syntax_db)) {
        syntax = &(*syntax);
    }
}

Editor::Vec2 Editor::calc_cursor() {
    size_t x = 1; // terminal coords start at 1, 1
    if (yloc() < lines.size()) {
        const auto &chars = lines[yloc()].chars;
        x = accumulate(begin(chars) + offset.x, begin(chars) + xloc(), x,
                       [](auto x, auto c) {
                           return x + (c == Keycode::TAB
                                           ? tab_size - (x % tab_size)
                                           : 1);
                       });
    }
    return {int(x), yloc() + 1};
}

void Editor::refresh_screen() {
    ostringstream ost;
    ost << EC_CURSOR_HIDE << EC_CURSOR_HOME;
    for_each(begin(lines),
             begin(lines) + min(lines.size(), size_t(screen_size.y)),
             [&](auto &line) {
                 int color = -1;
                 auto riter = begin(line.rendered) + offset.x;
                 auto riter_end = riter + min(line.rendered.size() - offset.x, 
                                              size_t(screen_size.x));
                 auto hiter = begin(line.highlights) + offset.x;
                 for (; riter != riter_end; ++riter, ++hiter) {
                     if (*hiter == Highlight::unprintable) {
                         ost << EC_TERM_FORMAT_INVERSE
                             << (*riter < 27 ? char(int('@') + int(*riter)) : '?')
                             << EC_TERM_FORMAT_RESET;
                     } else if (*hiter == Highlight::normal) {
                         if (color != -1) {
                             ost << EC_TERM_FORMAT_FORE_COLOR_RESET;
                             color = -1;
                         }
                         ost << *riter;
                     } else {
                         if (const int c = highlight_to_color(*hiter);
                             c != color) {
                             color = c;
                             ost << "\x1b[" << color << "m"; // code to set fore color
                         }
                         ost << *riter;
                     }
                 }
		      ost << EC_TERM_FORMAT_FORE_COLOR_RESET << EC_TERM_CLEAR_LINE_RIGHT;
            });
    for(size_t i = lines.size(); i < screen_size.y; ++i) {
        ost << "~" << EC_TERM_FORMAT_CLEAR_LINE_RIGHT << "\r\n";
    }
    // first row status
    ost << EC_TERM_CLEAR_LINE_RIGHT << EC_TERM_FORMAT_INVERSE;
    string status = filename + " " + to_string(lines.size()) + " lines " +
                    (dirty ? "(modified)" : "");
    status = status.substr(0, min(size_t(screen_size.x), status.size()));
    const auto right = to_string(yloc() + 1) + "/" + to_string(lines.size());
    if (status.size() < screen_size.x - right.size() - 1) {
        fill_n(back_inserter(status), screen_size.x - status.size() - right.size(), ' ');
        status += right;
    }
    ost << status << EC_TERM_FORMAT_RESET << "\r\n";
    const auto cursor = calc_cursor();

    // DEBUG
    // set_status_message(to_string(cursor.x) + ", " + to_string(cursor.y) +
    //                    " | len = " + to_string(lines.size()));

    // second row status
    ost << EC_TERM_CLEAR_LINE_RIGHT;
    // TODO: Auto timeout message:
    if (not statusmsg.empty()) { // and time(nullptr) - statusmsg_time < 5) {
        ost << (statusmsg.size() > screen_size.x ? statusmsg.substr(0, screen_size.x)
                                                 : statusmsg);
    }
    // set cursor location
    ost << "\x1b[" << cursor.y << ';' << cursor.x << "H" << EC_TERM_CURSOR_SHOW;
    const auto full = ost.str();
    write(STDOUT_FILENO, full.c_str(), full.size());
}

void Editor::move_cursor(int key) {
    auto x = offset.x + cursor.x, y = offset.y + cursor.y;
    switch (key) {
    case Keycode::ARROW_LEFT:
        if (cursor.x > 0) {
            --cursor.x;
        } else if(offset.x > 0) {
            --offset.x;
        } else if(y > 0) { // jump to end of prev line
            --cursor.y;
            auto line = begin(lines) + y - 1;
            cursor.x = min(line->chars.size(), size_t(screen_size.x - 1));
            offset.x = line->chars.size() -
                       min(size_t(screen_size.x + 1), line->chars.size());
        }
        break;
    case Keycode::ARROW_RIGHT:
        if (y < lines.size()) {
            if (x < lines[y].chars.size()) {
                move_right();
            } else if (x == lines[y].chars.size()) {
                cursor.x = 0;
                offset.x = 0;
                move_down();
            }
        }
        break;
    case Keycode::ARROW_UP:
        if (y > 0) { move_up(); }
        break;
    case Keycode::ARROW_DOWN:
        if (y < lines.size()) { move_down(); }
        break;
    }
    x = offset.x + cursor.x;
    y = offset.y + cursor.y;
    const auto n = (lines.size() > y ? lines[y].chars.size() : 0);
    if (x > n) {
        cursor.x -= x - n;
        if (cursor.x < 0) {
            offset.x += cursor.x;
            cursor.x = 0;
        }
    }
}

void Editor::proc_key(int key) {
    switch(key) {
    case Keycode::ENTER:         
        insert_newline();
        break;
    case Keycode::CTRL_C:        
        break;
    case Keycode::CTRL_Q:     
        run = false;
        break;
    case Keycode::CTRL_S: 
        save(filename);
        break;
    case Keycode::CTRL_F:
        // find();
        break;
    case Keycode::CTRL_H:     
    case Keycode::BACKSPACE:    
    case Keycode::DEL:
        erase_char();
        break;
    case Keycode::PAGE_UP:
    case Keycode::PAGE_DOWN:
        break;
    case Keycode::ARROW_UP:
    case Keycode::ARROW_DOWN:
    case Keycode::ARROW_LEFT:
    case Keycode::ARROW_RIGHT:
        move_cursor(key);
        break;
    default:
        insert_char(key);
        break;
    }
}
