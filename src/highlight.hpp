/*
MIT License

Copyright (c) 2019 Jeremy Jurksztowicz

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#ifndef _ec_highlight_hpp_
#define _ec_highlight_hpp_

#include <vector>
#include <string>

namespace ec {

enum class Highlight : int {
    normal, unprintable, comment, long_comment, keyword1, keyword2, string,
    number, emphasis};

struct Syntax {
    std::vector<std::string> file_extensions;
    std::vector<std::string> keywords;
    std::string comment_begin, multi_comment_begin, multi_comment_end;
};

std::vector<Syntax> const& syntax_db();

constexpr int highlight_to_color(Highlight h) {
    switch (h) {
    case Highlight::comment:
    case Highlight::long_comment:   return 36; // cyan
    case Highlight::keyword1:       return 33; // yellow
    case Highlight::keyword2:       return 32; // green
    case Highlight::string:         return 35; // magenta
    case Highlight::number:         return 31; // red
    case Highlight::emphasis:       return 34; // blue
    default:                        return 37; // white
    }
}

}
#endif