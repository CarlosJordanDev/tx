#ifndef _ec_terminal_hpp
#define _ec_terminal_hpp

#include <termios.h>
#include "chars.hpp"

namespace ec {

#define EC_TERM_FORMAT_RESET            "\u001b[0m"
#define EC_TERM_FORMAT_BOLD             "\u001b[1m"
#define EC_TERM_FORMAT_UNDERLINE        "\u001b[4m"
#define EC_TERM_FORMAT_INVERSE          "\u001b[7m"
#define EC_TERM_FORMAT_FORE_COLOR(n)    ("\u001b[38;5;" #n "m")
#define EC_TERM_FORMAT_BACK_COLOR(n)    ("\u001b[48;5;" #n "m")
#define EC_TERM_FORMAT_FORE_COLOR_RESET "\u001b[39m"
#define EC_TERM_CURSOR_UP(n)            ("\u001b[" #n "A")
#define EC_TERM_CURSOR_DOWN(n)          ("\u001b[" #n "B")
#define EC_TERM_CURSOR_RIGHT(n)         ("\u001b[" #n "C")
#define EC_TERM_CURSOR_LEFT(n)          ("\u001b[" #n "D")
#define EC_TERM_CURSOR_LINES_UP(n)      ("\u001b[" #n "E")
#define EC_TERM_CURSOR_LINES_DOWN(n)    ("\u001b[" #n "F")
#define EC_TERM_CURSOR_SET_COLUMN(n)    ("\u001b[" #n "G")
#define EC_TERM_CURSOR_SET_COORDS(x, y) ("\u001b[" #x ";" #y "H")
#define EC_TERM_CURSOR_HOME		        "\u001b[H"
#define EC_TERM_CURSOR_SAVE             "\u001b7"
#define EC_TERM_CURSOR_RESTORE          "\u001b8"
#define EC_TERM_CURSOR_HIDE             "\u001b[?25l"
#define EC_TERM_CURSOR_SHOW             "\u001b[?25h" 
#define EC_TERM_CLEAR_LINE_RIGHT        "\u001b[0K"
#define EC_TERM_CLEAR_LINE_LEFT         "\u001b[1K"
#define EC_TERM_CLEAR_LINE_ALL          "\u001b[2K"

namespace Keycode {
enum Keycode {
    NULLKEY 	= 0, 
    CTRL_C 	= 3, 
    CTRL_D 	= 4, 
    CTRL_F 	= 6, 
    CTRL_H 	= 8, 
    TAB 	= 9, 
    CTRL_L 	= 12, 
    ENTER 	= 13, 
    CTRL_Q 	= 17, 
    CTRL_S 	= 19, 
    CTRL_U 	= 21, 
    ESC 	= 27, 
    BACKSPACE 	= 127, 
// software codes:
    ARROW_LEFT 	= 9000, 
    ARROW_RIGHT, 
    ARROW_UP, 
    ARROW_DOWN, 
    DEL, 
    HOME, 
    END, 
    PAGE_UP, 
    PAGE_DOWN};
}

class Terminal {
public:
   ~Terminal() { restore(); }
    Terminal() = default;
    Terminal(Terminal const&) = delete;
    Terminal& operator=(Terminal const&) = delete;
    void restore();
    void enable_raw_mode();
    bool is_in_raw_mode() const { return _in_raw_mode; }
    int read_key();
    std::pair<size_t, size_t> get_size() const;
private:
    termios _orig_termios; 
    bool _in_raw_mode = false;
};

}
#endif
