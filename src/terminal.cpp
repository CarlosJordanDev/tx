#include "terminal.hpp"
#include <cassert>
#include <stdexcept>
#include <errno.h>
#include <unistd.h>
#include <sys/ioctl.h>

using namespace ec;
using namespace std;

void Terminal::restore() {
    if (_in_raw_mode) {
        tcsetattr(STDOUT_FILENO, TCSAFLUSH, &_orig_termios);
        _in_raw_mode = false;
    }
}

void Terminal::enable_raw_mode() {
    assert(not _in_raw_mode);
    if (not isatty(STDIN_FILENO)) {
        throw runtime_error("No TTY available.");
    }
    errno = 0;
    if (tcgetattr(STDIN_FILENO, &_orig_termios) < 0) {
        throw runtime_error(strerror(errno));
    }
    auto raw = _orig_termios;
    raw.c_iflag &= ~(BRKINT | ICRNL | INPCK | ISTRIP | IXON);
    raw.c_oflag &= ~(OPOST);
    raw.c_cflag |= CS8;
    raw.c_lflag &= ~(ECHO | ICANON | IEXTEN | ISIG);
    raw.c_cc[VMIN] = 0;
    raw.c_cc[VTIME] = 1;
    errno = 0;
    if (tcsetattr(STDIN_FILENO, TCSAFLUSH, &raw) < 0) {
        throw runtime_error(strerror(errno)); 
    }
    _in_raw_mode = true;
}

int Terminal::read_key() {
    int num_read = 0;
    char c = 0;
    char seq[3] = {0, 0, 0};
    do {
        num_read = read(STDIN_FILENO, &c, 1);
    } while (num_read < 1);
    if (num_read == -1) {
        throw runtime_error("Terminal::read_key failure!");
    }
    while (true) {
        if (c != Keycode::ESC) {
            return c;
        } else if (read(STDIN_FILENO, seq, 1) == 0) {
            return Keycode::ESC;
        } else if (read(STDIN_FILENO, seq + 1, 1) == 0) {
            return Keycode::ESC;
        } else if (seq[0] == '[') {
            if (seq[1] >= '0' and seq[1] <= '9') {
                // extended escape
                if (read(STDIN_FILENO, seq + 2, 1) == 0) {
                    return Keycode::ESC;
                } else if (seq[2] == '~') {
                    switch (seq[1]) {
                    case '3':
                        return Keycode::DEL;
                    case '5':
                        return Keycode::PAGE_UP;
                    case '6':
                        return Keycode::PAGE_DOWN;
                    }
                }
            } else {
                switch (seq[1]) {
                case 'A':
                    return Keycode::ARROW_UP;
                case 'B':
                    return Keycode::ARROW_DOWN;
                case 'C':
                    return Keycode::ARROW_RIGHT;
                case 'D':
                    return Keycode::ARROW_LEFT;
                case 'H':
                    return Keycode::HOME;
                case 'F':
                    return Keycode::END;
                }
            }
            // esc 0 sequence
        } else if (seq[0] == 'O') {
            switch (seq[1]) {
            case 'H':
                return Keycode::HOME;
            case 'F':
                return Keycode::END;
            }
        }
    }
}

pair<size_t, size_t> Terminal::get_size() const {
    winsize ws;
    if (ioctl(1, TIOCGWINSZ, &ws) == -1 or ws.ws_col == 0) {
        return {0, 0};
    } else {
        return {ws.ws_col, ws.ws_row};
    }
}
