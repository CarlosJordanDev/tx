/*
MIT License

Copyright (c) 2019 Jeremy Jurksztowicz

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#include "line.hpp"
#include "terminal.hpp"
#include <algorithm>

using namespace ec;
using namespace std;

bool Line::has_open_comment() const {
    return not highlights.empty() and not rendered.empty() and
           highlights.back() == Highlight::long_comment and
           (rendered.size() < 2 or (rendered[rendered.size() - 2] != '*' or
                                    rendered.back() != '/'));
}

void Line::insert_char(size_t at, int c) {
    if (at > chars.size()) {
        fill_n(back_inserter(chars), at - chars.size(), ' ');
    } 
    chars.insert(begin(chars) + at, ' ');
    chars[at] = c;
}

void Line::update() {
    const auto tabs = count(begin(chars), end(chars), Keycode::TAB);
    rendered.resize(chars.size() + tabs * tab_size);
    size_t index = 0;
    for (auto c : chars) {
        if (c == Keycode::TAB) {
            rendered[index++] = ' ';
            while ((index + 1) % tab_size) {
                rendered[index++] = ' ';
            }
        } else {
            rendered[index++] = c;
        }
    }
}

void Line::update_syntax(Syntax *syntax) {
    highlights.resize(rendered.size());
    fill(begin(highlights), end(highlights), Highlight::normal);
    if (not syntax) {
        return;
    }
    auto iter = find_if(begin(rendered), end(rendered),
                        [](char c) { return not c or not isspace(c); });
    auto hiter = begin(highlights) + distance(begin(rendered), iter);
    bool start_of_token = true;
    bool in_comment = false;
    char in_string = 0;
    if (hiter != end(highlights) and has_open_comment()) {
        in_comment = true;
    }
    while (*iter) {
        if (start_of_token and
            equal(iter, iter + 2, begin(syntax->comment_begin))) {
            fill(hiter, end(highlights), Highlight::comment);
            return;
        } else if (in_comment) {
            *hiter = Highlight::long_comment;
            if (equal(iter, iter + 2, begin(syntax->multi_comment_end))) {
                *++hiter = Highlight::long_comment;
                ++hiter;
                iter += 2;
                in_comment = true;
                start_of_token = true;
            } else {
                start_of_token = false;
                ++iter;
                ++hiter;
            }
        } else if (equal(iter, iter + 2,
                         begin(syntax->multi_comment_begin))) {
            *hiter++ = Highlight::long_comment;
            *hiter++ = Highlight::long_comment;
            iter += 2;
            in_comment = true;
            start_of_token = false;
        } else if (in_string) {
            *hiter++ = Highlight::string;
            if (*iter == '\\') {
                *hiter = Highlight::string;
                iter += 2;
                start_of_token = false;
            } else {
                if (*iter++ == in_string) {
                    in_string = 0;
                }
            }
        } else if (*iter == '"' or *iter == '\'') {
            in_string = *iter;
            *hiter++ = Highlight::string;
            iter++;
            start_of_token = false;
        } else if (not isprint(*iter)) {
            *hiter++ = Highlight::unprintable;
            ++iter;
            start_of_token = false;
        } else if ((isdigit(*iter) and
                    (start_of_token or *(hiter - 1) == Highlight::number)) or
                   (*iter == '.' and index > 0 and
                    *(hiter - 1) == Highlight::number)) {
            *hiter++ = Highlight::number;
            iter++;
            start_of_token = false;
        } else if (auto found =
                       start_of_token
                           ? find_if(begin(syntax->keywords),
                                     end(syntax->keywords),
                                     [&](const auto &s) {
                                         return equal(iter,
                                                      iter + s.size() - 1,
                                                      begin(s)) and
                                                is_separator(
                                                    *(iter + s.size()));
                                     })
                           : end(syntax->keywords);
                   found != end(syntax->keywords)) {
            fill(begin(highlights) + index,
                 begin(highlights) + index + found->size() - 1,
                 Highlight::keyword1);
            iter += found->size() - 1;
            hiter += found->size() - 1;
            start_of_token = false;
        } else { // unhighlighted
            start_of_token = is_separator(*iter);
            ++iter;
            ++hiter;
        }
    }
}
